local guest = ...
local listOfFiles = {"startup", "displays", "jsonlib/localConfig", "jsonlib/globalConfig", "jsonlib/objectJSON", "jsonlib/jsonUpdate"}

for _,v in pairs(listOfFiles) do
	if fs.exists(v) then
		shell.run("rm " .. v)
	end
end

local function loadApis()
	-- DOWNLOAD & LOAD APIS	
	shell.run("pastebin get 4nRg9CHU jsonlib/json")
	HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/dev/jsonlib/objectJSON-linked.lua", "jsonlib/objectJSON")
	
	if guest then -- local config for a guest
		HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/dev/localConfigGuest.json", "localConfig")
		print("Guest config")
	else
		HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/dev/localConfig.json", "localConfig")
		print("Owner config")
	end

	HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/dev/globalConfig.json", "globalConfig")
	HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/dev/displays.lua", "displays")
	HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/dev/jsonlib/jsonUpdate.lua", "jsonlib/jsonUpdate")

	if fs.exists("staffCoJSON") then
		shell.run("rm staffCoJSON")
	end
	
	print("APIS : OK")
end

local h = fs.open("startup", "w")
h.write(http.get("https://gitlab.com/computercraftdaiki/terminalglasses/raw/dev/glasses.lua").readAll())
h.close()


if guest then
	shell.run("startup " .. guest)
else
	shell.run("startup")
end