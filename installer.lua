local guest = ...
local listOfFiles = {"startup", "displays", "jsonlib/localConfig", "jsonlib/globalConfig", "jsonlib/objectJSON", "jsonlib/jsonUpdate"}

for _,v in pairs(listOfFiles) do
	if fs.exists(v) then
		shell.run("rm " .. v)
	end
end

local h = fs.open("startup", "w")
h.write(http.get("https://gitlab.com/computercraftdaiki/terminalglasses/raw/master/glasses.lua").readAll())
h.close()

if guest then
	shell.run("startup " .. guest)
else
	shell.run("startup")
end