--- VARIABLES ---
local objLocalConfig = objectJSON.decodeFromFile("localConfig")
local objGlobalConfig = objectJSON.decodeFromFile("globalConfig")
local timeDisplay = objLocalConfig.timeDisplay
local playerInBaseDisplay = objLocalConfig.playerInBaseDisplay
local coDecoStaffDisplay = objLocalConfig.coDecoStaffDisplay

--- FUNCTIONS ---
-- display time
local event
local widthTime = 70
local textTime
local color = 0xD1AF04 -- default color (just in case)
function timeDis(glass)
	local IGtime = textutils.formatTime(os.time(), true)
	local t = os.time()
	if IGtime == nil or t == nil then
		print("Getting time or t failed, time = " .. tosting(IGtime) .. " and t = " .. tostring(t))
	end
	if (t > 6.0 and t < 6.27) then
		event = "SUNSHINE"
		color = 0xFF850A
	elseif (t > 6.27 and t < 17.37) then
		event = "DAY"
		color = 0xD1AF04
	elseif (t > 17.37 and t < 19.48) then
		event = "SUNSET"
		color = 0xFF3B0A
	elseif (t > 19.48 or t < 4.55) then
		event = "NIGHT"
		color = 0x0033FF
	elseif (t > 23.3 or t < 0.3) then
		event = "MIDNIGHT"
		color = 0x00209E
	end
	if event == nil then
		event = ""
	end
	textTime = event .. " - " .. tostring(IGtime)
	widthTime = math.floor(string.len(textTime) * 5) + 10
	glass.addBox(1, 20, widthTime, 10, 0xFFFFFF, 0.6)
	glass.addText(5, 22, textTime, color)
end


local colorPlayer
local widthPlayer = 70
local textPlayerInBase
-- display if a player is in the base
function isPlayerInBase(glass)
	if redstone.testBundledInput("front", colors.orange) then
		textPlayerInBase = "VISITOR : YES"
		colorPlayer = "0xFF0000"
	else
		textPlayerInBase = "VISITOR : NO"
		colorPlayer = "0x048C16"
	end
	widthPlayer = math.floor(string.len(textPlayerInBase) * 5.5)
	glass.addBox(1, 30, widthPlayer, 10, 0xFFFFFF, 0.6)
	glass.addText(5, 32, textPlayerInBase , tonumber(colorPlayer, 16))
end


-- returns the color associated a staff rank
local function colorPlayer(pseudo)
	for _,obj in pairs(objGlobalConfig.listStaff) do
		for _,player in pairs(obj.list) do
			if pseudo == player then
				return obj.color
			end
		end
	end
	return objLocalConfig.colorNonStaffPlayer
end

-- display all players connected on all servers and status
function glist(glass)
	local arrObj = objectJSON.listConnectedPlayers()
	if arrObj == nil then
		print("Warning : get player list on Mineaurion API failed.")
		return
	end
	local serveur
	local statut
	local nbPlayers
	local totPlayers = 0
	local tablePlayers
	local text
	local widthServInfo
	local colorServ = 0x0033FF
	local colorStatut
	local y = 50
	local yInit = y
	local x = 100
	local i = 1
	if arrObj == nil then
		print("Warning : was not able to get player list")
		return 1
	end
	for _,obj in pairs(arrObj) do
		-- get infos
		serveur		= obj.info
		statut		= obj.statut
		nbPlayers	= obj.players
		totPlayers	= totPlayers + nbPlayers
		arrPlayers	= obj.joueurs
		
		widthServInfo = 190
		nbPlayers = tostring(nbPlayers)
		if statut == "On" then
			colorStatut = 0x048C16
		else
			colorStatut = 0xAD0000
		end
		
		
		-- display infos
		glass.addBox(x, y, widthServInfo, 10, 0xFFFFFF, 0.6)
		glass.addText(x + 4, y + 2, string.upper(serveur) , 0x0033FF)
		glass.addText(x + 150, y + 2, string.upper(statut) , colorStatut)
		glass.addText(x + 170, y + 2, nbPlayers, colorServ)
		y = y + 10
		if arrPlayers ~= nil and arrPlayers ~= false then
			for _,pseudo in pairs(arrPlayers) do
				glass.addBox(x, y, widthServInfo, 10, 0xFFFFFF, 0.6)
				glass.addText(x + 4, y + 2, pseudo, tonumber(colorPlayer(pseudo), 16))
				y = y + 10
			end
		end
		y = y + 10
		i = i + 1
		if i == 3 then
			x = x + 200
			y = yInit
		end
	end
	glass.addBox(x - 12, yInit - 20, 16, 10, 0xFFFFFF, 0.6)
	glass.addText(x - 10, yInit - 18, tostring(totPlayers), 0xAD0000)
	glass.sync()
end


-- initialize the file containing the list of all members of the staff
function initStaffJSON(file)
	local h = fs.open(file, "w")
	h.writeLine("[")
	
	local arrListRanks = objGlobalConfig.listStaff
	for _,objRank in pairs(arrListRanks) do -- for each object rank
		for _,pseudo in pairs(objRank.list) do -- for each player
			h.writeLine("	{")
			h.writeLine("		\"pseudo\": \"" .. pseudo .. "\"")
			h.writeLine("		\"rank\": \"" .. objRank.rank .. "\"")
			h.writeLine("		\"isConnected\": false")
			h.writeLine("		\"server\": false")
			h.writeLine("	},")
		end
	end
	
	h.writeLine("]")
	h.close()
end

local listPlayerServer = {}
-- returns a table of all connected players associated with the server they are on
local function getListPlayerServer()
	listPlayerServer = {}
	local apiMineaurion = objectJSON.listConnectedPlayers()
	for _,obj in pairs(apiMineaurion) do
		local serv = obj.info
		local listPlayers = obj.joueurs
		if listPlayers ~= false and listPlayers ~= nil then
			for _,player in pairs(listPlayers) do
				listPlayerServer[player] = serv
			end
		end
	end
	return listPlayerServer
end

-- returns true if the player is connected
local function isConnected(pseudo)
	for player,_server_ in pairs(listPlayerServer) do
		if player == pseudo then
			return true
		end
	end
	return false
end

local oldStaffCoServ = {}
local staffCoServ = {}
local newCo = {}
local newDeco = {}
-- update informations about the staff that is connected, list newCo and list newDeco
local function updateStaffCo(file)
	local f = fs.open(file, "r")
	local objOldStaffList = objectJSON.decode(f.readAll())
	f.close()
	
	local h = fs.open(file, "w")
	h.writeLine("[")
	
	--objCoPlayers = objectJSON.listConnectedPlayers()
	listPlayerServer = getListPlayerServer()
	
	oldStaffCoServ = staffCoServ
	staffCoServ = {}
	-- actualize the file
	for _,obj in pairs(objOldStaffList) do -- list of each staff member object
		if isConnected(obj.pseudo, listPlayerServer) then -- if the staff member is connected AMELIORABLE par: not listPlayerServer[obj.pseudo] == nil
			staffCoServ[obj.pseudo] = listPlayerServer[obj.pseudo]
			h.writeLine("	{")
			h.writeLine("		\"pseudo\": \"" .. obj.pseudo .. "\"")
			h.writeLine("		\"rank\": \"" .. obj.rank .. "\"")
			h.writeLine("		\"isConnected\": true")
			h.writeLine("		\"server\": \"" .. listPlayerServer[obj.pseudo] .. "\"")
			h.writeLine("	},")
		else -- if the staff member isn't connected
			h.writeLine("	{")
			h.writeLine("		\"pseudo\": \"" .. obj.pseudo .. "\"")
			h.writeLine("		\"rank\": \"" .. obj.rank .. "\"")
			h.writeLine("		\"isConnected\": false")
			h.writeLine("		\"server\": false")
			h.writeLine("	},")
		end
	end
	h.writeLine("]")
	h.close()
	
	newCo = {}
	newDeco = {}
	local f = fs.open(file, "r")
	local arrObj = objectJSON.decode(f.readAll())
	f.close()
	
	for _,obj in pairs(arrObj) do
		 -- the player just connected
		if oldStaffCoServ[obj.pseudo] == nil and obj.isConnected == true then
			print("[" .. obj.server .. "] : +" .. obj.pseudo)
			newCo["+" .. obj.pseudo] = obj.server
		 -- the player just disconnected
		elseif oldStaffCoServ[obj.pseudo] ~= nil and obj.isConnected == false then
			print("[" .. oldStaffCoServ[obj.pseudo] .. "] : -" .. obj.pseudo)
			newDeco["-" .. obj.pseudo] = oldStaffCoServ[obj.pseudo]
		end
	end
end

-- convert a server complete name to an alias
local function servAlias(servName)
	local arrObj = objGlobalConfig.listServ
	for _,objServ in pairs(arrObj) do
		if servName == objServ.name then
			return objServ.alias
		end
	end
	return servName -- if no aliased had been found in config file
end

local y
-- displays staff member that connects and disconnects from a server
local function coDecoStaff(glass)
	updateStaffCo("staffCoJSON")
	if newCo ~= {} or newDeco ~= {} then
		y = 50
		for player,server in pairs(newCo) do
			glass.addBox(1, y, 150, 10, 0xFFFFFF, 0.6)
			glass.addText(5, y + 2, "[" .. servAlias(server) .. "] : ", 0x048C16)
			glass.addText(32, y + 2, player, tonumber(colorPlayer(string.sub(player, 2)), 16))
			y = y + 10
		end
		for player,server in pairs(newDeco) do
			glass.addBox(1, y, 150, 10, 0xFFFFFF, 0.6)
			glass.addText(5, y + 2, "[" .. servAlias(server) .. "] : ", 0x0033FF)
			glass.addText(32, y + 2, player, tonumber(colorPlayer(string.sub(player, 2)), 16))
			y = y + 10
		end
		glass.sync()
		sleep(2)
	end
end


-- display all commands avaible
function helpEvt(commandList, commandPerm, glass)
	local y = 50
	for index, command in ipairs(commandList) do
		if commandPerm[index] then
			glass.addBox(0, y, 105, 10, 0xFFFFFF, 0.6)
			glass.addText(4, y + 2, index .. " - " .. command, 0xFC6203)
			y = y + 10
		end
    end
	glass.sync()
end

-- troll people that aren't whitelisted
function troll(glass)
	updateDisplay = false
	local color = 0xBB0000
	while trollActive do
		if color == 0xBB0000 then
			color = 0x00BB00
		else
			color = 0xBB0000
		end
		glass.clear()
		local trollText = playerWearingGlasses .. " EST UN GROS NAZE HAHAHAHAHAHA"
		glass.addBox(0, 0, 1000, 1000, color, 1)
		glass.addText(250, 202, trollText, 0x000000)
		glass.sync()
		sleep(1)
	end
	updateDisplay = true
end

local updateMeter = 0
-- call all displayers
function callDisplays(glass, updateDisplay)
	if updateDisplay then
		glass.clear()
		
		if timeDisplay then
			timeDis(glass)
		end
		
		if playerInBaseDisplay then
			isPlayerInBase(glass, updateMeter, updateFrequency)
		end
		
		if updateMeter % 10 == 0 and coDecoStaffDisplay then -- update every 10 seconds
			coDecoStaff(glass, updateMeter, updateFrequency)
		end
		glass.sync()
		updateMeter = updateMeter + 1
	end
end