local function newKeyOldVal(oldTable, newTable)
    local finalTable = {}
    local oldVal
    for newKey,newVal in pairs(newTable) do
        oldVal = oldTable[newKey]
        if oldVal == nil then
            -- case key wasn't in the old table
            finalTable[newKey] = newVal
        else
            -- case key was in the old table
            if type(oldVal) == "table" then
                finalTable[newKey] = newKeyOldVal(oldVal, newVal)
            else
                finalTable[newKey] = oldVal
            end
        end
    end
    return finalTable
end

function updateJson(oldConfFile, newConfFile, finalConfFile)
    local oldConf = objectJSON.decodeFromFile(oldConfFile)
    local newConf = objectJSON.decodeFromFile(newConfFile)
    local finalConf = newKeyOldVal(oldConf, newConf)
    if type(finalConf) ~= "table" or finalConf == {} then
        error("Updating local configuration failed.")
    end
    objectJSON.encodeToFile(finalConf, finalConfFile)
end
