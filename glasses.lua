--- GLOBAL VARIABLES ---
local guest = ...			-- if the argument is true, then adapt the config for a guest
local isGuest				-- bool : if true then it is a guest installation
local objLocalConfig		-- json object containing the client config
local objGlobalConfig		-- json object containing the server config
local whitelist				-- array of players authorized to use the glasses
local listStaff = {}		-- table of all members of the staff by rank
local listStaffCo = {}		-- table of connected staff
local glass					-- terminal glasses object
local updateDisplay = true	-- update display or not
local playerWearingGlasses = ""	-- string : current player that is wearing the glasses -> actually the last player that put the glasses on
local trollActive = false	-- boolean : determine if the troll function is activated or not
local commandList = {}		-- table : index associated with a command
local commandPerm = {}		-- table : (command) index associated with their permission
local evt					-- last event that occured
local bundleCableFace		-- string : side where the bundle cable is
local timeOut = 1000		-- timeout if no sundial is present (~50secs)
local displayLogs = true	-- if true then display logs (events that occur, informations...)
local filesToUpdate = {"startup", "displays", "jsonlib/objectJSON", "globalConfig"}

--- USEFUL FUNCTIONS ---
-- helper function and constants
local function pack(...)
    return {...}
end

-- returns if the pseudo is whitelisted
local function isWhistelisted (pseudo)
    for _,v in ipairs(whitelist) do
        if v == pseudo then
            return true
        end
    end
    return false
end

-- save the content of an HTTP link into a file
local function HTTPSaveToFile(link, filename)
	local h = fs.open(filename, "w")
	h.write(http.get(link).readAll())
	h.close()
end


--- INIT & UPDATE ---
-- first run
local function init()
	local mon = peripheral.find("monitor")
	if mon ~= nil then
		term.redirect(mon)
	end
	
	if not fs.exists("startup") then
		shell.run("label set terminal_glasses")
	end

end

-- update the program
local function checkUpdate()
	if fs.exists("test") then -- don't  update if test file is present
		print("TEST mode : no update")
		return
	end
	local obj = json.decode(http.get("https://gitlab.com/computercraftdaiki/terminalglasses/raw/master/globalConfig.json").readAll())
	if obj.version > objGlobalConfig.version then
		print("")
		print("NEW VERSION AVAIBLE")
		print("UPDATE from version " .. objGlobalConfig.version .. " to version " .. obj.version)
		-- remove old files
		for _,filename in pairs(filesToUpdate) do
			shell.run("rm " .. filename)
		end
		-- update of the local CONFIG
		if isGuest then
			HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/master/localConfigGuest.json", "newLocalConfig")
		else
			HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/master/localConfig.json", "newLocalConfig")
		end
		jsonUpdate.updateJson("localConfig", "newLocalConfig", "finalLocalConfig")
		shell.run("rm localConfig")
		shell.run("rm newLocalConfig")
		shell.run("mv finalLocalConfig localConfig")
		print("Local config updated")
		-- prepare for reboot with new files
		HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/master/glasses.lua", "startup")
		print("REBOOT")
		sleep(1)
		shell.run("reboot")
	end
	print("UPDATE : OK")
end


-- display the actual version
local function version()
	print("[V" .. objGlobalConfig.version .."] Terminal glasses display by DaikiKaminari")
	print("")
end


local function loadApis()
	-- DOWNLOAD & LOAD APIS	
	if not fs.exists("jsonlib/json") then
		shell.run("pastebin get 4nRg9CHU jsonlib/json")
	end
	os.loadAPI("jsonlib/json")

	if not fs.exists("jsonlib/objectJSON") then
		HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/master/jsonlib/objectJSON-linked.lua", "jsonlib/objectJSON")
	end
	os.loadAPI("jsonlib/objectJSON")
	
	if not fs.exists("localConfig") then
		if guest then -- local config for a guest
			HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/master/localConfigGuest.json", "localConfig")
			print("Guest config")
		else
			HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/master/localConfig.json", "localConfig")
			print("Owner config")
		end
	end

	if not fs.exists("globalConfig") then
		HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/master/globalConfig.json", "globalConfig")
	end
	
	if not fs.exists("displays") then
		HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/master/displays.lua", "displays")
	end
	os.loadAPI("displays")

	if not fs.exists("jsonlib/jsonUpdate") then
		HTTPSaveToFile("https://gitlab.com/computercraftdaiki/terminalglasses/raw/master/jsonlib/jsonUpdate.lua", "jsonlib/jsonUpdate")
	end
	os.loadAPI("jsonlib/jsonUpdate")

	if fs.exists("staffCoJSON") then
		shell.run("rm staffCoJSON")
	end
	
	print("APIS : OK")
end

-- initialize global variables and load config
local function loadConfig()
	-- CONFIG
	objLocalConfig = objectJSON.decodeFromFile("localConfig")

	objGlobalConfig = objectJSON.decodeFromFile("globalConfig")

	isGuest = objLocalConfig.guest
	
	whitelist = objLocalConfig.whitelist
	
	listStaff = objGlobalConfig.listStaff

	displayLogs = objLocalConfig.displayLogs
	
	for _,obj in pairs(objLocalConfig.commandList) do
		commandList[obj.index] = obj.command
		commandPerm[obj.index] = obj.activated
	end
	
	
	for _,v in pairs(objGlobalConfig.listStaff) do
		listStaffCo[v] = false
	end
	
	-- VARIABLES
	glass = peripheral.wrap(objLocalConfig.terminalBridgeFace)
	if glass == nil then
		print("Warning : no side configurated for terminal bridge, set to default : left.")
		glass = peripheral.wrap("left")
	end
	if glass == nil then
		error("Terminal Glasses Bridge not found")
	end
	glass.clear()
	glass.sync()

	bundleCableFace = objLocalConfig.bundledCableFace
	if bundleCableFace == null then
		print("Warning : no side configurated for bundled cable, set to default : back.")
		bundleCableFace = "back"
	end
	redstone.setBundledOutput(bundleCableFace, 0) -- set redstone signals to false

	print("CONFIG : OK")
end


--- OVERLOADED FUNCTIONS ---
local function _troll()
	displays.troll(glass)
end

local function _callDisplays()
	while true do
		displays.callDisplays(glass, updateDisplay)
		sleep(1)
	end
end


--- FUNCTIONS ---
-- command parser
local function onEventDo(evt)
	-- if event is putting on the glasses
	if evt[1] == "glasses_attach" then
		if displayLogs then
			print("     EVENT : " .. evt[1])
			print("PLAYER : " .. evt[3])
		end
		playerWearingGlasses = evt[3]
		if isWhistelisted(evt[3]) then
			updateDisplay = true
		else
			trollActive = true
			parallel.waitForAll(_troll, catchEvent)
		end
	end
	
	-- if event is a command
	if evt[1] == "glasses_chat_command" then
		if displayLogs then
			print("     EVENT : " .. evt[1])
			print("COMMAND : " .. evt[5] .. " by " .. evt[3])
		end
		if isWhistelisted(evt[3]) then
			if (evt[5] == commandList[1] or evt[5] == "1") and commandPerm[1] then -- weather clear : remove rain from overworld
				redstone.setBundledOutput(bundleCableFace, colors.yellow)
				sleep(1)
			elseif (evt[5] == commandList[2] or evt[5] == "2") and commandPerm[2] then -- time set day : accelerate time until morning
				local t = 0
				while os.time() < 7 or os.time() > 8 do
					if t > timeOut then
						print("Warning : Timeout, Sun Dial not present ?")
						break
					end
					redstone.setBundledOutput(bundleCableFace, colors.orange)
					sleep(0)
					t = t + 1
				end
			elseif (evt[5] == commandList[3] or evt[5] == "3") and commandPerm[3] then -- time set night : accelerate time until night
				local t = 0
				while os.time() < 19 or os.time() > 20 do
					if t > timeOut then
						print("Warning : Timeout, Sun Dial not present ?")
						break
					end
					redstone.setBundledOutput(bundleCableFace, colors.orange)
					sleep(0)
					t = t + 1
				end
			elseif (evt[5] == commandList[4] or evt[5] == "4") and commandPerm[4] then -- time set midnight : accelerate time until midnight
				local t = 0
				while os.time() < 23.5 and os.time() > 0.5 do
					if t > timeOut then
						print("Warning : Timeout, Sun Dial not present ?")
						break
					end
					redstone.setBundledOutput(bundleCableFace, colors.orange)
					sleep(0)
					t = t + 1
				end
			elseif (evt[5] == commandList[5] or evt[5] == "5") and commandPerm[5] then -- color : activate particules generators
				redstone.setBundledOutput(bundleCableFace, colors.white)
				sleep(1)
			elseif (evt[5] == commandList[6] or evt[5] == "6") and commandPerm[6] then -- expulsion : activate blood magic rituals
				redstone.setBundledOutput(bundleCableFace, colors.red)
				sleep(1)
			elseif (evt[5] == commandList[7] or evt[5] == "7") and commandPerm[7] then -- alarm : activate alarms
				redstone.setBundledOutput(bundleCableFace, colors.green)
				sleep(1)
			elseif (evt[5] == commandList[8] or evt[5] == "8") and commandPerm[8] then -- ban : same thing as color + expulsion + alarm
				redstone.setBundledOutput(bundleCableFace, colors.white)
				redstone.setBundledOutput(bundleCableFace, colors.red)
				redstone.setBundledOutput(bundleCableFace, colors.green)
				sleep(1)
			elseif (evt[5] == commandList[9] or evt[5] == "9") and commandPerm[9] then -- shutdown : cut nearly all systems
				redstone.setBundledOutput(bundleCableFace, colors.cyan)
				sleep(1)
			elseif (evt[5] == commandList[10] or evt[5] == "10") and commandPerm[10] then -- glist : display infos about servers
				updateDisplay = false
				displays.glist(glass)
				sleep(10)
				updateDisplay = true
			elseif evt[5] == "help" or evt[5] == "" then -- help : display all commands
				updateDisplay = false
				displays.helpEvt(commandList, commandPerm, glass)
				sleep(6)
				updateDisplay = true
			else
				print("Info : command not attributed")
			end
			redstone.setBundledOutput(bundleCableFace, 0)
		end
	end	
	return
end

-- event catcher
local function catchEvent()
	while true do
		evt = pack(os.pullEvent())
		-- if event is putting off the glasses
		if evt[1] == "glasses_detach" then
			if displayLogs then
				print("     EVENT : " .. evt[1])
				print("PLAYER : " .. evt[3])
			end
			trollActive = false
		else
			onEventDo(evt)
		end
	end
end


--- MAIN ---
local function main()
	shell.run("clear")
	init()
	loadApis()
	loadConfig()
	checkUpdate()
	sleep(3)
	shell.run("clear")
	version()
	redstone.setBundledOutput(bundleCableFace, 0)
	displays.initStaffJSON("staffCoJSON")
	glass.clear()
	sleep(6)
	while true do
		parallel.waitForAll(_callDisplays, catchEvent)
	end
end
 
main()